# wp_podcast_api

A WordPress Endpoint that defines the endpoints I need for my podcast tools.

This pluin needs several other plugins to work correctly. Setting up with Lando makes sure that the environment is 100% setup and running the way you need it to be. If you don't want to use lando for dev, install `wp-cli` and look at the commands necessary to setup and activate plugins in the `.lando.yml` file.

## Installation

- Install [Lando](https://lando.dev/)
- clone this repo
- cd into this repo's directory
- `lando start`
- [Dev Site](https://podcast.lndo.site/)

The plugin should now be ready to use. You can make sure it is operational by pointing a browser at `https://podcast.lndo.site/wp-json`. In the `namespaces` portion of the payload, you should see `eicc/podcasts/v1` This lets you know that the namespace has been registered.
