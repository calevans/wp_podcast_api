<?php
/**
 * Controller for the Episode endpoint
 *
 * This class registers three new endpoints for the WordPress
 * REST API. These endpoints are used to manage WordPress posts by podcast
 * episode number instead of POST_ID.
 *
 * This is the sample code for "Extending the WordPress REST API" By Cal Evans
 *
 * @author Cal Evans <cal@calevans.com
 *
 * @todo migrate most of this into models (thin controller, fat model)
 */
class WP_Podcast_API_Episode extends WP_REST_Controller {


	const VENDOR  = 'eicc';
	const PACKAGE = 'podcasts';
	const ENTITY  = 'episodes';
	const VERSION = 1;

	/**
	 * The namespace
	 *
	 * @var string $namespace
	 */
	protected $namespace = '';

	/**
	 * The root of the meta key
	 *
	 * @var string $meta_key_base
	 */
	protected $meta_key_base = '';

	/**
	 * The type of post we are manipulating
	 *
	 * @var string $post_type
	 */
	protected $post_type = 'post';

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->namespace = self::VENDOR .
		'/' . self::PACKAGE .
		'/v' . self::VERSION;

		$this->uri = '/' . self::ENTITY;

		$this->meta_key_base = '_' . self::VENDOR .
		'_' . self::PACKAGE .
		'_' . self::ENTITY;
	}

	public function initialization() {
		$this->register_routes();
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		/*
	 	 * /episode
		 */
		register_rest_route(
			$this->namespace,
			$this->uri,
			array(
				array(
					'methods'  => WP_REST_Server::READABLE,
					'callback' => array( $this, 'get_items' ),
					'args'     => $this->get_args_schema(),
				),
				array(
					'methods'             => WP_REST_Server::CREATABLE,
					'callback'            => array( $this, 'create_item' ),
					'permission_callback' => array( $this, 'create_item_permissions_check' ),

				),
			)
		);

		/*
		 * /episode/1234
		 */
		register_rest_route(
			$this->namespace,
			$this->uri . '/(?P<episode_id>[\d]+)',
			array(
				array(
					'methods'  => WP_REST_Server::READABLE,
					'callback' => array( $this, 'get_item' ),
					'schema'   => array( $this, 'get_item_schema' ),
					'args'     => array(
						'episode_id' => array(
							'description'       => esc_html__( 'Unique identifier for the episode.', 'wp_podcast_api' ),
							'type'              => 'integer',
							'sanitize_callback' => array( $this, 'sanitize_int' ),
							'required'          => true,
						),
					),
				),
				array(
					'methods'             => WP_REST_Server::EDITABLE,
					'callback'            => array( $this, 'update_item' ),
					'permission_callback' => array( $this, 'update_item_permissions_check' ),
					'schema'              => array( $this, 'get_item_schema' ),
					'args'                => array(
						'episode_id' => array(
							'description'       => esc_html__( 'Unique identifier for the episode.', 'wp_podcast_api' ),
							'type'              => 'integer',
							'sanitize_callback' => array( $this, 'sanitize_int' ),
							'required'          => true,
						),
					),
				),
				array(
					'methods'             => WP_REST_Server::DELETABLE,
					'callback'            => array( $this, 'delete_item' ),
					'permission_callback' => array( $this, 'delete_item_permissions_check' ),
					'schema'              => array( $this, 'get_item_schema' ),
					'args'                => array(
						'episode_id' => array(
							'description'       => esc_html__( 'Unique identifier for the episode.', 'wp_podcast_api' ),
							'type'              => 'integer',
							'sanitize_callback' => array( $this, 'sanitize_int' ),
							'required'          => true,
						),
					),
				),
			)
		);

		/*
		 * /schema
		 */
		register_rest_route(
			$this->namespace,
			$this->uri . '/schema',
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_item_schema' ),
			)
		);
	}


	/**
	 * Prepares the item for the REST response.
	 *
	 * This puts everything in the proper format so that WP_REST_Server and output
	 * it.
	 *
	 * @param mixed           $item WordPress representation of the item.
	 * @param WP_REST_Request $request Request object.
	 * @return WP_Error|WP_REST_Response Response object on success, or WP_Error object on failure.
	 */
	public function prepare_item_for_response( $item, $request ) {
		$item->meta_info = array();
		$meta            = get_post_meta( $item->ID );

		foreach ( $meta as $key => $value ) {
			$key                     = preg_replace( '/' . $this->meta_key_base . '_/', '', $key );
			$item->meta_info[ $key ] = $value[0];
		}
		$item = (array) $item;

		$response = rest_ensure_response( $item );
		$server   = rest_get_server();
		$links    = $server::get_compact_response_links( $response );

		$links['collection'] = array(
			'href' => rest_url( sprintf( '%s/%s', $this->namespace, $this->rest_base ) ),
		);

		$response->add_links( $links );
		return $response;
	}


	/**
	 * Gather all published episodes that meet the criteria. If no criteria is
	 * passed then the last 10 episodes published will be returned.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_items( $request ) {
		$data = array();

		$args = array(
			'meta_key'       => $this->meta_key_base . '_id',
			'orderby'        => array(
				'meta_value_num' => 'DESC',
			),
			'meta_value'     => 0,
			'meta_compare'   => '>=',
			'posts_per_page' => $request->get_param( 'posts_per_page' ),
			'paged'          => $request->get_param( 'paged' ),
		);

		$query = new WP_Query( $args );

		foreach ( $query->posts as $post ) {
			$post     = $this->filter_response_by_context( $post, 'view' );
			$response = $this->prepare_item_for_response( $post, $request );
			$data[]   = $this->prepare_response_for_collection( $response );
		}

		$final = rest_ensure_response( $data );
		$final->set_status( 200 );

		return $final;
	}

	/**
	 * Retrieve a single episode.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_item( $request ) {
		$episode_id   = $request->get_param( 'episode_id' );
		$this_episode = $this->fetch_episode( $episode_id );

		if ( is_null( $this_episode ) ) {
			return new WP_Error(
				'cant-find',
				__( 'Cannot find episode', 'wp_podcast_api' ),
				array( 'status' => 404 )
			);
		}
		$final = $this->prepare_item_for_response( $this_episode, $request );
		$final->set_status( 200 );

		return $final;
	}

	/**
	 * Returns a single episode or null if no episode exists.
	 *
	 * @param int $episode_id The episode (NOT POST) id to fetch.
	 * @return object|null
	 */
	protected function fetch_episode( int $episode_id ) : ?object {
		$args = array(
			'post_status' => array( 'publish' ),
			'post_count'  => 1,
			'meta_key'    => $this->meta_key_base . '_id',
			'meta_value'  => $episode_id,
		);

		if ( current_user_can( 'edit_posts' ) ) {
			$args['post_status'][] = 'draft';
			$args['post_status'][] = 'future';
		}

		$posts = ( new WP_Query( $args ) )->posts;

		if ( count( $posts ) < 1 ) {
			return null;
		}

		return $posts[0];
	}

	/**
	 * Get the post, if the ID is valid.
	 *
   * This retrieves a single post using a WordPress post_id.
	 * @param int $id Supplied ID.
	 * @return WP_Post|WP_Error Post object if ID is valid, WP_Error otherwise.
	 */
	protected function get_post( $id ) {
		$error = new WP_Error( 'rest_post_invalid_id', __( 'Invalid post ID.' ), array( 'status' => 404 ) );
		if ( (int) $id <= 0 ) {
			return $error;
		}

		$post = get_post( (int) $id );
		if ( empty( $post ) || empty( $post->ID ) || $this->post_type !== $post->post_type ) {
			return $error;
		}

		return $post;
	}


	/**
	 * Returns true if this episode is already in the system
   *
   * This check to see if there is a Wordpress post with the passed in episode
   * id attached to it. It returns true if it finds a post.
	 *
	 * @param int $episode_id The id of the episode to check.
	 */
	protected function episode_exists( int $episode_id ): bool {
		return ! empty( $this->fetch_episode( $episode_id ) );
	}

	/**
	 * Create an episode
	 *
	 * This will check to make sure the episode does not already exist and return
	 * a 409 if it does.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function create_item( $request ) {
		$data       = $request->get_params();
		$episode_id = $data['meta_input']['episode_id'];

		if ( $this->episode_exists( $episode_id ) ) {
			return new WP_Error(
				'cant-create',
				__( 'Episode already exists.', 'wp_podcast_api' ),
				array( 'status' => 409 )
			);
		}

		$post                  = $this->prepare_item_for_database( $request );
		$post->post_category   = array();
		$post->post_category[] = get_cat_ID( $data['post_category'] ?? 'podcast' );

		$post->meta_input['enclosure'] = ( ! empty( $data['meta_input']['enclosure'] ) )
		? $data['meta_input']['enclosure']
		: null;

		$post_id = wp_insert_post( $post );

		if ( 0 === $post_id ) {
			return new WP_Error(
				'cannot-create',
				__(
					'Something went wrong, the post was not created.',
					'wp_podcast_api'
				),
				array( 'status' => 500 )
			);
		}

		$this->remove_enclosure( $post_id );
		$new_post = $this->get_post( $post_id );

		$response = $this->prepare_item_for_response( $new_post, $request );
		$response->set_status( 201 );

		return $response;
	}

	/**
	 * Update an episode
   *
   * This will update an existing WordPress post that is associated with the
   * episode id passed in if there is one.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Request
	 */
	public function update_item( $request ) {
		$data       = $request->get_params();
		$episode_id = $data['meta_input']['episode_id'];

		if ( ! $this->episode_exists( $episode_id ) ) {
			return new WP_Error(
				'cant-update',
				__( 'Episode does not exist.', 'wp_podcast_api' ),
				array( 'status' => 404 )
			);
		}

		$data = $request->get_params();

		$post                = $this->prepare_item_for_database( $request );
		$post->post_category = array(
			get_cat_ID( $data['post_category'] ?? 'podcast' ),
		);

		$post->ID = $this->fetch_episode( $episode_id )->ID;

		$post->meta_input['enclosure'] = ( ! empty( $data['meta_input']['enclosure'] ) ) ?
		$data['meta_input']['enclosure'] : null;

		$post_id = wp_update_post( $post, true );

		if ( is_wp_error( $post_id ) ) {
			return $post_id;
		}

		if ( 0 === $post_id ) {
			return new WP_Error(
				'cant-update',
				__( 'Something went wrong, the post was not created.', 'wp_podcast_api' ),
				array( 'status' => 500 )
			);
		}

		$this->remove_enclosure( $post_id );
		$new_post = $this->fetch_episode( $post_id );

		$response = $this->prepare_item_for_response( $new_post, $request );
		$response->set_status( 200 );

		return $response;
	}


	/**
	 * Delete one post
   *
   * This will delete a WordPress post based on the episode_id passed it. If
   * it finds an associated post, it will delete it. Otherwise it will return
   * an error.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Request
	 */
	public function delete_item( $request ) {
		$data       = $request->get_params();
		$episode_id = $data['meta_input']['episode_id'];

		if ( ! $this->episode_exists( $episode_id ) ) {
			return new WP_Error(
				'cant-delete',
				__( 'Episode does not exist.', 'wp_podcast_api' ),
				array( 'status' => 404 )
			);
		}
		$post_id = $this->fetch_episode( $episode_id )->ID;

		$post = wp_delete_post( $post_id, true );

		if ( false === $post ) {
			return new WP_Error(
				'cant-delete',
				__( 'Something went wrong with the delete.', 'wp_podcast_api' ),
				array( 'status' => 500 )
			);
		}

		$response = $this->prepare_item_for_response( $post, $request );
		$response->set_status( 200 );
	}

  /**
   * Remove the _encloseme record for a given post that is associated with a
   * podcast episode.
   *
   * This is REQUIRED to keep WordPress from deleting the enclosure because
   * the podcast URL is not actually in the post.
   */
	protected function remove_enclosure( $post_id ) {
		global $wpdb; // Yuk.

		$wpdb->query(
			$wpdb->prepare(
				"DELETE
           FROM {$wpdb->postmeta}
          WHERE meta_key = %s
                and post_id = %d",
				array(
					'_encloseme',
					$post_id,
				)
			)
		);
	}

	/**
	 * Takes the data that comes in from the request and maps it to the
	 * WordPress array necessary to add or update.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 *
	 * NOTE: We do not validate that all the necessary data is here,
	 *       just that the pieces passed in are in the schema.
	 *
	 * @return object
	 */
	protected function prepare_item_for_database( $request ) {
		$post = new StdClass();

		$schema           = $this->get_item_schema();
		$post->meta_input = array();

		$data = $request->get_params();

		foreach ( $data as $key => $value ) {
			if ( 'meta_input' === $key ) {
				foreach ( $value as $meta_key => $meta_value ) {
					if ( ! isset( $schema['properties'][ $meta_key ] ) ) {
						continue;
					}

					$meta_key                      = ( 'episode_id' === $meta_key ) ? 'id' : $meta_key;
					$property                      = $this->meta_key_base . '_' . $meta_key;
					$post->meta_input[ $property ] = $meta_value;
				}
			}

			if ( ! isset( $schema['properties'][ $key ] ) ) {
				continue;
			}

			$post->$key = $value;
		}

		/*
		 * Edge Cases
		 */
		$post->meta_input['_yoast_wpseo_twitter-image'] = ( ! empty( $data['meta_input']['twitter_card_url'] ) )
		? ( $data['meta_input']['twitter_card_url'] . '?' . time() )
		: null;

		$post->meta_input['_yoast_wpseo_opengraph-image'] = ( ! empty( $data['meta_input']['facebook_graphic_url'] ) )
		? ( $data['meta_input']['facebook_graphic_url'] . '?' . time() )
		: null;

		$post->post_author = $post->post_author > 0
		? $post->post_author
		: get_current_user_id();

		return $post;
	}

	/**
	 * Check if the validated user of this request has access to create an episode
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|bool
	 */
	public function create_item_permissions_check( $request ): bool {
		return current_user_can( 'edit_posts' );
	}

	/**
	 * Check if the validated user of this request has access to update an episode
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|bool
	 */
	public function update_item_permissions_check( $request ): bool {
		return $this->create_item_permissions_check( $request );
	}

	/**
	 * Check if the validated user of this request has access to delete an episode
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|bool
	 */
	public function delete_item_permissions_check( $request ): bool {
		return $this->create_item_permissions_check( $request );
	}

	/**
	 * The query params for listings endpoint
   *
	 * @return array
	 */
	public function get_args_schema() {
		return array(
			'paged'          => array(
				'description'       => 'Current page of the collection.',
				'type'              => 'integer',
				'required'          => false,
				'default'           => 1,
				'sanitize_callback' => array( $this, 'sanitize_int' ),
			),
			'posts_per_page' => array(
				'description'       => 'Maximum number of items to be returned in result set.',
				'required'          => false,
				'type'              => 'integer',
				'default'           => 10,
				'sanitize_callback' => array( $this, 'sanitize_int' ),
			),
			'context'        => array(
				'description' => 'Context',
				'required'    => false,
				'type'        => 'string',
				'default'     => 'view',
			),
		);
	}

	/**
	 * Returns the schema of the endpoints
	 *
	 * @return array
	 */
	public function get_item_schema() {
		if ( $this->schema ) {
			return $this->add_additional_fields_schema( $this->schema );
		}

		$schema = array(
			'$schema'    => 'http://json-schema.org/draft-04/schema#',
			'title'      => self::ENTITY,
			'type'       => 'object',
			'properties' => array(
				'episode_id'      => array(
					'description' => esc_html__(
						'Unique identifier for the episode.',
						'wp_podcast_api'
					),
					'type'        => 'integer',
					'context'     => array( 'view', 'edit', 'episode' ), // since We created episode, we have to list them all.
				),
				'artist'          => array(
					'description' => esc_html__(
						'The name of the guest for this episode.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'album'           => array(
					'description' => esc_html__(
						'The name of the podcast.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'title'           => array(
					'description' => esc_html__(
						'The title of the episode.',
						'wp_podcast_api'
					),
					'type'        => 'string',
					'context'     => array( 'view', 'edit', 'episode', 'embed' ),
				),
				'genre'           => array(
					'description' => esc_html__(
						'The genre of the podcast.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'publisher'       => array(
					'description' => esc_html__(
						'The holder of the copyright.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'composer'        => array(
					'description' => esc_html__(
						'The host of the podcast.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'year'            => array(
					'description' => esc_html__(
						'The year this episode was recorded.',
						'wp_podcast_api'
					),
					'type'        => 'integer',
				),
				'image'           => array(
					'description' => esc_html__(
						'The logo for the podcast.',
						'wp_podcast_api'
					),
					'type'        => 'string',
					'context'     => array( 'view', 'edit', 'episode', 'embed' ),
				),
				'twitterHandle'   => array(
					'description' => esc_html__(
						'The Twitter handle for the guest.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'twitterCardUrl'  => array(
					'description' => esc_html__(
						'The URL for the Twitter card for this episode.',
						'wp_podcast_api'
					),
					'type'        => 'string',
					'context'     => array( 'view', 'edit', 'episode' ),
				),
				'facebookCardUrl' => array(
					'description' => esc_html__(
						'The URL for the facebook card for this episode.',
						'wp_podcast_api'
					),
					'type'        => 'string',
					'context'     => array( 'view', 'edit', 'episode' ),
				),
				'audioUrl'        => array(
					'description' => esc_html__(
						'The URL for the audio file for this episode.',
						'wp_podcast_api'
					),
					'type'        => 'string',
					'context'     => array( 'view', 'edit', 'episode' ),
				),
				'shownotes'       => array(
					'description' => esc_html__(
						'Array of title and url for the show notes for this episode.',
						'wp_podcast_api'
					),
					'type'        => 'array',
				),
				'retired'         => array(
					'description' => esc_html__(
						'If true, this episode has been retired and is no longer editable.',
						'wp_podcast_api'
					),
					'type'        => 'boolean',
				),
				'post_content'    => array(
					'description' => esc_html__(
						'The post body.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'post_title'      => array(
					'description' => esc_html__(
						'The post title.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'post_author'     => array(
					'description' => esc_html__(
						'The alternative id of the post author. (Default is the logged in user)',
						'wp_podcast_api'
					),
					'type'        => 'integer',
				),
				'comment_status'  => array(
					'description' => esc_html__(
						'Are comments open or closed.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'ping_status'     => array(
					'description' => esc_html__(
						'Are pings open or closed.',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'post_category'   => array(
					'description' => esc_html__(
						'The post category. (Default is podcast)',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
				'tags_input'      => array(
					'description' => esc_html__(
						'Comma delimited list of tags. (Default is the artist name)',
						'wp_podcast_api'
					),
					'type'        => 'string',
				),
			),
		);

		$this->schema = $schema;

		return $this->add_additional_fields_schema( $this->schema );
	}


	/**
	 * Just make sure that it is an int.
	 *
	 * @param mixed $value Whatever value you want to pass in as an int.
	 * @return int
	 */
	public function sanitize_int( $value ): int {
		return filter_var( $value, FILTER_SANITIZE_NUMBER_INT );
	}
}
