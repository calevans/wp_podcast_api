<?php
/**
 * Plugin Name:     wp_podcast_api
 * Plugin URI:      https://gitlab.com/calevans/wp_podcast_apiE
 * Description:     Defines the API endpoints necessary for Voices of the ElePHPant
 * Author:          Cal Evans
 * Author URI:      https://blog.calevans.com
 * Text Domain:     wp_podcast_api
 * Domain Path:     /languages
 * Version:         0.2.0
 *
 * @package         wp_podcast_api
 */

/**
 * Here's what I am thinking
 * Endpoint /podcast/episode
 * CRUD
 * List
 *
 * Right now I am going to keep episodes as regular posts. Eventually, I
 * could see them as a custom post type.
 *
 * Episode Properties
 * - episode INT
 * - artist STRING
 * - album STRING
 * - title STRING
 * - genre STRING
 * - publisher STRING
 * - composer STRING
 * - year INT
 * - image STRING
 * - twitterHandle STRING
 * - audio url STRING
 * - twitter card url STRING
 * - facebook graphic url STRING
 * - show notes ARRAY (JSON ENCODED?)
 * - retired BOOL
 * - sponsor logo url STRING
 */
require_once dirname( __FILE__ ) . '/class_wp_podcast_api_episode.php';

add_action( 'rest_api_init', 'wp_podcast_api_init_route' );

/**
 * Kick things off
 */
function wp_podcast_api_init_route() {
	( new WP_Podcast_API_Episode() )->initialization();
}

/*
 * The new JWT Plugin by default blacklists everything except the auth API.
 * So temporarily, we are whitelisting everything. This doesn't mean you can
 * get to API endpoints without auth that should require auth, it just means
 * that you can get to the ones that don't require auth without auth.
 */
add_filter(
	'jwt_auth_whitelist',
	function ( $endpoints ) {
		return array(
			'/wp-json/*',
		);
	}
);

